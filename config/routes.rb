Myproductiveday::Application.routes.draw do
  root "people#index"

  resource :settings, only: [:show, :update]
  resources :people, except: [:new]
  resources :reports, only: [:index, :show]
end
