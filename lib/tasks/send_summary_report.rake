 desc "Sends summary report to all people"
 task :send_daily_summary_report do
   people = Person.all
   report = Report.summary.last

   people.each do |person|
     ApplicationMailer.daily_summary_report(person, report)
   end
 end
