 desc "Sends email to all people - what you've done today'"
 task :send_email do
   people = Person.all

   people.each do |person|
     ApplicationMailer.what_youve_done_today(person)
   end
 end
