require 'net/pop'

desc "Generates report base on answers inbox"
task :generate_report => :environment do
 pop = Net::POP3.new 'mail.isp.com'
 pop.start 'username@isp.com', 'password'

  if pop.mails.empty?
    return
  else
    pop.mails.each_with_index do |m,i|
    File.open( "inbox/#{i}", 'w+' ) do|f|
      f.write m.pop
    end

    m.delete
  end
 end
end
