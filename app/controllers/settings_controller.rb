class SettingsController < ApplicationController
  before_action :set_settings

  def show
  end

  def update
    @settings.update_attributes(settings_params)

    redirect_to settings_path
  end

  private

    def set_settings
      @settings = Settings.first
    end

    def settings_params
      params.require(:settings).permit(:title, :text)
    end
end
