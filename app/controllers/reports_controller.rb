class ReportsController < ApplicationController
  def index
    @reports = Report.summary
  end

  def person
    @reports = Person.find(params[:person_id]).reports
  end

  def show
    @report = Report.find(params[:id])
  end
end
