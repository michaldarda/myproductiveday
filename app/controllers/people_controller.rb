class PeopleController < ApplicationController
  before_action :set_person, except: [:create, :new, :index]

  def index
    @people = Person.all

    @person = Person.new
  end

  def create
    @person = Person.new(person_params)

    if @person.save
      redirect_to people_path, notice: "Person successfully added"
    else
      redirect_to people_path, alert: "Can't create a new person"
    end
  end

  def new
    @person = Person.new
  end

  def show
  end

  def destroy
    if @person.destroy
      redirect_to people_path, notice: "Person successfully deleted"
    else
      redirect_to people_path, alert: "Sorry, can't delete a person"
    end
  end

  def update
    if @person.update_attributes(person_params)
      redirect_to people_path, notice: "Person successfully updated"
    else
      redirect_to people_path, alert: "Can't update a person"
    end
  end

  private

  def person_params
    params.require(:person).permit(:email)
  end

  def set_person
    @person = Person.find(:id)
  end
end
