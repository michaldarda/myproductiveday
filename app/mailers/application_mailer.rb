class ApplicationMailer < ActionMailer::Base
  def what_youve_done_today(user)
    settings = Settings.first
    mail(
         to: user,
         subject: settings.title
        ) do |format|
      format.text { render settings.text }
    end
  end

  def daily_summary_report(user)
    last_summary_report = Report.summary.last

    mail(
         to: user,
         subject: "Summary report for #{DateTime.today}"
        ) do |format|
      format.text { render last_summary_report.text }
    end
  end
end
