class Report < ActiveRecord::Base
  belongs_to :person
  belongs_to :summary_report, :foreign_key => 'parent_report_id', :class_name => "Report"
  has_many   :person_reports, :foreign_key => 'parent_report_id', :class_name => "Report"

  scope :summary, -> { where(parent_report_id: nil) }
end
