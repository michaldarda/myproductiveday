class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :person_id
      t.integer :parent_report_id
      t.text    :text

      t.timestamps
    end
  end
end
